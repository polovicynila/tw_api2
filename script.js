const API_KEY = "524ff0ca-042c-493b-bfee-a776a873c92c"
const API_URL_POPULAR = "https://kinopoiskapiunofficial.tech/api/v2.2/films/top?type=TOP_100_POPULAR_FILMS&page=1"
const API_URL_SEARCH = "https://kinopoiskapiunofficial.tech/api/v2.1/films/search-by-keyword?keyword="
const form = document.querySelector('form')
const search = document.querySelector('.header__search')


getMovies(API_URL_POPULAR)

form.addEventListener("submit", (e)=>{
    e.preventDefault()
    const apiSeearchUrl = `${API_URL_SEARCH}${search.value}`;
    if (search.value) {
      getMovies(apiSeearchUrl);

      search.value = ""
    }
})


async function getMovies(url){
    const responce = await fetch(url,  {
     headers:{
        "content-type": "application/json",
        "X-API-KEY": API_KEY,
     }   
    })
    const responceData = await responce.json()
    console.log(responceData);
    showMovies(responceData)
}




function getClassByRate(vote){
  if(vote >= 7){
    return 'green'
  }else if (vote > 5){
    return 'orange'
  }else if (vote > 4){
    return 'red'
  }else{
    return 'none'
  }
    
}

function showMovies(data) {
    const moviesElement = document.querySelector(".movies")

    document.querySelector(".movies").innerHTML = "";
    
    data.films.forEach((movie) =>  {
      const movieElement = document.createElement('div')
      movieElement.classList.add('movie')
      movieElement.innerHTML = `<div class="movie">
      <div class="movie__cover-inner">
        <img class="movie__cover" src="${movie.posterUrlPreview}" alt="${movie.nameRu}">
        <div class="movie__cover--darkened"></div>
      </div>
      <div class="movie__info">
        <div class="movie__title">${movie.nameRu}</div>
        <div class="movie__title">${movie.filmLength}</div>
        <div class="movie__title">${movie.year}</div>
        <div class="movie__category">${movie.genres.map((genre) => `${genre.genre}`)}</div>
        <div class="movie__average movie__average--${getClassByRate(
          movie.rating
        )}">${
          movie.rating
        }</div>
      </div>
    </div>`
    moviesElement.appendChild(movieElement)
    });
}
console.log('hi')